let balanceAmount = document.getElementById("balance");
let loanAmount = document.getElementById("loan");
const getLoanButton = document.getElementById("get-loan");
let payAmount = document.getElementById("pay");
const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");

const laptopsElement = document.getElementById("laptop-name");
const laptopTitle = document.getElementById("laptop-title");
let laptopImage = document.getElementById("laptop-image");
let laptopFeatures = document.getElementById("laptop-features");
const laptopDescription = document.getElementById("laptop-description");
const laptopPrice = document.getElementById("laptop-price");
const buyButton = document.getElementById("buy");

let laptops = [];
let pay = 0;
let balance = 0;
let totalLoan = 0;
let hasLoan = false;

// Work & Bank

const transferSalaryToBalance = (e) => {
    e.preventDefault();
    if(hasLoan) {
        console.log(totalLoan);
        totalLoan -= 0.1 * pay;
        balance += 0.9 * pay;
    } else {
        balance += pay;
    }
    balanceAmount.innerText = balance;
    pay = 0;
    payAmount.innerText = pay;
    loanAmount.innerText = totalLoan;
}

// no negative loan possible (not working yet)
// turn hasLoan to false, when totalLoan is 0. (not working yet)

const increasePayBalance = (e) => {
    e.preventDefault();
    pay = pay + 100;
    payAmount.innerText = pay;
}

const handleGetLoan = () => { 
    totalLoan = prompt("Please enter the amount of money you want to loan:");
    const maxLoan = 2 * balance;
    if(hasLoan) {
        window.alert("You already have a loan");
        return;
    } else if(totalLoan > maxLoan) {
        window.alert("Your loan can't be bigger than two times your balance amount");  
        totalLoan = 0;
    } else {
        balance += parseFloat(totalLoan);
        hasLoan = true;
    }
    balanceAmount.innerText = balance;
    loanAmount.innerText = totalLoan;
}

getLoanButton.addEventListener("click", handleGetLoan);
workButton.addEventListener("click", increasePayBalance);
bankButton.addEventListener("click", transferSalaryToBalance);

// Computer store

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToOptions(laptops));


const addLaptopsToOptions = (laptops) => {
    laptops.forEach(x => addLaptopToOptions(x));
    laptopPrice.innerText = laptops[0].price + " NOK";
    laptopDescription.innerText = laptops[0].description;
    laptopTitle.innerText = laptops[0].title;
}

const addLaptopToOptions = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    laptopTitle.innerText = selectedLaptop.title;
    // laptopImage.innerText = selectedLaptop.image;
    // console.log(laptopImage);
    // laptopFeatures.innerText = selectedLaptop.specs;
    laptopPrice.innerText = selectedLaptop.price;
    laptopDescription.innerText = selectedLaptop.description; 

    const createListOfFeatures = selectedLaptop.specs.map((feature) => {
        // for(i = 0; i < feature.length; i++) {
        //     console.log(feature[i]);

        // }
        // console.log(feature[0]);
        return feature;
    })

    // let laptopFeaturesList = selectedLaptop.specs.map((feature) => {
    //     return `<li>${feature}</li>`;
    // })

    // const laptopFeature = document.createElement("li");
    // laptopFeatures = document.createElement("li");
    // laptopFeatures.innerText = createListOfFeatures;

    // laptopFeatures = selectedLaptop.map((feature) => {
    //     return (<li>feature.specs</li>)
    // })


    laptopFeatures.innerText = createListOfFeatures;
    // laptopFeatures.innerText = selectedLaptop.specs;
}

const handleBuyLaptop = () => { // doesn't work yet
    if(balance <= laptopPrice) {
        window.alert("You can't afford the laptop");    
    } else {
        console.log(laptopPrice);
        window.alert("You're now the owner of the new laptop");
        balance -= laptopPrice;  
    }
    balanceAmount.innerText = balance;
}

laptopsElement.addEventListener("change", handleLaptopChange);
buyButton.addEventListener("click", handleBuyLaptop);
